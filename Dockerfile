FROM node:14-alpine3.15



ADD package.json /tmp/package.json
RUN cd /tmp && npm install && npm rebuild node-sass
RUN mkdir -p /opt/app && cp -a /tmp/node_modules /opt/app/
WORKDIR /opt/app
ADD . /opt/app
ENV CHOKIDAR_USEPOLLING=true



EXPOSE 8080
ENTRYPOINT [ "npm" ]
CMD [ "run", "serve" ]

